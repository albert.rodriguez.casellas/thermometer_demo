FROM python:3.7-alpine
MAINTAINER Albert Rodriguez Casellas

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /orbitaldemo
WORKDIR /orbitaldemo
COPY ./orbitaldemo /orbitaldemo

RUN adduser -D dockuser
RUN chown dockuser:dockuser -R /orbitaldemo/
USER dockuser

