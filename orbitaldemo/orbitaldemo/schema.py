import graphene
import thermometer.schema

#Definition of the Query and Mutation objects defined in the thermometer class

class Query(thermometer.schema.Query, graphene.ObjectType):
    pass

class Mutation(thermometer.schema.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)