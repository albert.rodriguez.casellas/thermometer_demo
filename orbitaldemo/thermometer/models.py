from django.db import models
from django.utils import timezone

#Class created for every record of temperature
class Thermometer(models.Model):
    timestamp = models.DateTimeField(default=timezone.now)
    value = models.FloatField()
    unit = models.TextField(blank=True)