from django.shortcuts import render

from thermometer import schema
from .models import Thermometer
import graphene
import random

# Create your views here.



def index(request):
    """
    Función vista para la página inicio del sitio.
    """

    #Piece of code that makes the query to find the last record into the database

    '''Thermometer.objects.create(value=random.randint(0,50), unit="Celsius")
    #instance = Thermometer.objects.get(id=1)
    #instance.delete()

    schema_t = graphene.Schema(query=schema.Query)
    query = """
        query {
            currentTemperature {
                id = 
                timestamp
                value
                unit
            }
        }
    """
    result = schema_t.execute(query)'''


    #Piece of code for the mutation stuff (We've defined Celsius as predetermined but it can be replaced in 
    #the mutation)

    schema_t = graphene.Schema(mutation=schema.Mutation)
    mutation = '''
    mutation {
        createTemperature(
            unit: "Celsius"
        ) {
            id
            timestamp
            value
            unit
        }
    }
    '''
    result = schema_t.execute(mutation)
    print(result.data)
    print(result.data['createTemperature']['value'])

    '''If needed, we can see the number of records registered in the db by the following sentence (and 
    adding it to context)
    num_registers=Thermometer.objects.all().count()'''
    
    #The return renders the index.html template with the data gathered in the context values
    return render(
        request,
        'index.html',
        context={'register': result.data['createTemperature']['value'],'unit':result.data['createTemperature']['unit']},
    )

