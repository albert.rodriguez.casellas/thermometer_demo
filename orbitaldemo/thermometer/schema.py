from graphene_django import DjangoObjectType
from django.utils import timezone
from django.db import models
from .models import Thermometer
import random
import graphene

#Metadata 
class ThermometerType(DjangoObjectType):
    class Meta:
        model = Thermometer

#Query graphQL definition
class Query(graphene.ObjectType):
    thermometer = graphene.List(ThermometerType)
    currentTemperature = graphene.Field(ThermometerType)

    def resolve_thermometer(self, info, **kwargs):
        return Thermometer.objects.all()

    def resolve_currentTemperature(self, info, **kwargs):
        return Thermometer.objects.last()


#Record insertion graphQL definition
class CreateTemperature(graphene.Mutation):
    #Parameters for the new record
    id = graphene.Int()
    timestamp = graphene.types.datetime.DateTime()
    value = graphene.Float()
    unit = graphene.String()

    # Defines the data you can send to the server
    class Arguments:
        value = graphene.Float()
        unit = graphene.String()

    #Mutation definition
    def mutate(self, info, unit):
        thermo = Thermometer(value=random.randint(0,50), unit=unit)
        thermo.save()

        #Info returned when executing the mutation
        return CreateTemperature(
            id=thermo.id,
            timestamp=thermo.timestamp,
            value=thermo.value,
            unit=thermo.unit,
        )


#Creates a mutation class with a field to be resolved, which points to our mutation defined before
class Mutation(graphene.ObjectType):
    create_temperature = CreateTemperature.Field()