from django.test import TestCase
from thermometer import schema
from .models import Thermometer
from django.db import models
from graphene_django import DjangoObjectType
from graphene import ObjectType, Field, Schema
from django.utils import timezone
import graphene
import random


# Create your tests here.

class MyFancyTestCase(TestCase):
    # Here we inject our test case's schema (variables definition; temperature defined between 0 and 50 degrees)
    GRAPHQL_SCHEMA = schema
    val = random.randint(0,50)
    unit = "Celsius"

    def setUp(self):
        Thermometer.objects.create(value=self.val, unit=self.unit)


    def test_query_fields(self):

        schema_t = graphene.Schema(query=schema.Query)
        query = """
            query {
                currentTemperature {
                    id
                    timestamp
                    value
                    unit
                }
            }
        """
        
        result = schema_t.execute(query)
        print(result.data)

        #Some verifications to know whether the test has succeeded or not
        self.assertEqual(result.data['currentTemperature']['value'],self.val)
        self.assertEqual(result.data['currentTemperature']['unit'],self.unit)

    def test_mutation_fields(self):

        schema_t = graphene.Schema(mutation=schema.Mutation)
        mutation = '''
        mutation {
            createTemperature(
                unit: "Celsius"
            ) {
                id
                timestamp
                value
                unit
            }
        }
        '''
        result = schema_t.execute(mutation)
        print(result.data)

        self.assertEqual(result.data['createTemperature']['unit'],"Celsius")


        