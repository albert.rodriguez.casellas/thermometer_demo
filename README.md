# Thermometer_demo

This project aims to simulate the data gathering of a thermometer. 
- The project has a Django application as a backend (and an HTML file working as the frontend).
- The requests language is GraphQL, and SQLite is the type of database used.
- The whole project has been Dockerised to make the installation easier.

## Installation

It is assumed that Docker (Toolbox or Desktop) is already installed in the computer. The steps to follow are :

1- Create a folder somewhere in your computer, open the console in that directory and clone the project (git clone https://gitlab.com/albert.rodriguez.casellas/thermometer_demo)
2- Go inside the folder: cd thermometer_demo
3- Inside that folder there are all the Docker files. The Docker file creates the image and the compose creates the service (open ports, redirections, etc.)
4- Build the image: docker build .
5- Run the compose: docker-compose build
6- Run the server: docker-compose run orbitaldemo sh -c "python manage.py runserver 0.0.0.0:8000"
7- Open your favorite navigator and type localhost:8000/thermometer/index and you should see the thermometer page!


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate. Regarding the tests, you can type: docker-compose run orbitaldemo sh -c "python manage.py test thermometer" to run the tests created in thermometer/tests.py

